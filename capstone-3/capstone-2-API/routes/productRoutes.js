const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController.js')

const auth = require("../auth.js");


router.post("/addProduct", auth.verify, (req, res) => {

	// const userData = auth.decode(req.headers.authorization)
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});





// Get all products

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

// Get All Active courses
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// retrieve specific course
router.get("/:productId", (req, res) =>{
	console.log(req.params.productId);
		productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
								//////req.params nasa URL
})


// Update a specific course
router.put("/updateProduct/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
									//////req.params nasa URL

})


// archieve a course
router.put("/:productId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archieveProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
									//////req.params nasa URL

})





module.exports = router;
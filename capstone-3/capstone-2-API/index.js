// Server creation and DB connection

const express = require("express");

const mongoose = require("mongoose");
// Allows us to control the App's Cross origin Resource
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
// const orderRoutes = require("./routes/orderRoutes.js");

const app = express();


// MongoDB connection using SRV Link

mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.rw9m5pa.mongodb.net/CAPSTONE2API?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// optional - validation of DB connection
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."));




// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines rthe "/users" string to be included foir the user routes defined in the "user.js" route file.
app.use("/users", userRoutes);


app.use("/products", productRoutes);





// PORT LISTENING
if(require.main === module){
	app.listen(process.env.PORT || 4000, () => console.log(`API is now online ${process.env.PORT || 4000}`));
}



module.exports = app;
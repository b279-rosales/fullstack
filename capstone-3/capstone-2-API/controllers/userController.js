const User = require('../models/Users');

const bcrypt = require('bcrypt');

const auth = require('../auth');

const Product = require('../models/Products');


//Check if the email already exist

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};


module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,

		password: bcrypt.hashSync(reqBody.password, 10)
			 // bcrypt.hashSync encrypts the password , 10 = minimum salt rounds for encryption
	})


	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});

};

module.exports.getProfile = (reqBody) => {

    return User.findById(reqBody).then(result => {
        if (result == null) {
            return false;
        }else {
            result.password = ""

            return result;
        }
    })

};



// function to login a user.
module.exports.loginUser = (reqBody) => {

	return  User.findOne({email: reqBody.email}).then(result => {
		console.log(result)
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,
				result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else true
		}
	})

};






						// async
module.exports.checkout = async (data) => {
    if(!data.isAdmin){
        let productTemp = [];
        let totalAmountTemp = 0;
        let isProductUpdated;
        let products = data.products

        products.forEach(singleProduct => {
            console.log(singleProduct);
            Product.findById(singleProduct.productId).then(product => { 

            	console.log(product)

                productTemp.push(
                    {
                        productId : singleProduct.productId,
                        productName : product.name,
                        quantity : singleProduct.quantity
                    }
                )
                totalAmountTemp = totalAmountTemp + (product.price * singleProduct.quantity)
            }
            )
            isProductUpdated = Product.findById(singleProduct.productId).then(product =>{

                product.userOrders.push({userId : data.userId})
                return product.save().then((product, error) => {
                    if(error){
                        return false;
                    } else {
                        return true;
                    }
                })
            })
        });


        let isUserUpdated = await User.findById(data.userId).then(user => {
                user.orderedProduct.push(
                    {

                        product : productTemp,
                        totalAmount : totalAmountTemp
                    }
                );
                return user.save().then((user, error) => {
                    if(error){
                        return false;
                    }else{
                        return true ;
                    }
                })
        })


        if(isUserUpdated && isProductUpdated){
            return true;
        }else{
            return false;
        }
    } else {
        return Promise.resolve("Admins can't checkout products")
    }
};

///////////////////////////////////////////////////////////////////////

module.exports.getUserProfile = (reqParams, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){	

    return User.findById(reqParams.userId).then(result => {
        if (result == null) {
            return false;
        }else{
            result.password = ""

            return result;
        
	}
    })
	}else{
		let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
        return value
    })
	}
}

/////=======================SG
//

//// SETTING IS ADMIN STATUS TRUE/FALSE
module.exports.setAsAdmin = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let changeUser = {
		isAdmin : reqBody.isAdmin
		}

		return User.findByIdAndUpdate(reqParams.userId, changeUser).then((user, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}else{
		let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
        return value
    })
	}
}

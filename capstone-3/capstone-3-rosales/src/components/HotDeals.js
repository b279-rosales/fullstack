import { Row, Col, Card } from "react-bootstrap";

export default function HotDeals(){

	return(
		<Row className="my-3">
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>Receive Your Motorcycle at Home</h2>
                        </Card.Title>
                    </Card.Header>
                        <Card.Text>
                            We deliver your dream bike motorcycle at your doorstep
                            </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>Buy Now, Pay Later</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text>
    Buy now, pay later (BNPL) is a type of short-term financing. These loans are also called point-of-sale (POS) installment loans. Consumers can make purchases and pay for them over time after an up-front payment. Buy now, pay later plans typically charge no interest.            </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                        <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>Easy Pay, with your E-Wallet</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text>
                           Digital wallets allow for faster, more convenient, and more secure transactions. They can help reduce the risk of fraud and other security threats. Digital wallets can also help users manage their finances more effectively. They are increasingly popular among consumers.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
	)
}
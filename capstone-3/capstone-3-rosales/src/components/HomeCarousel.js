import {Carousel, Image} from 'react-bootstrap';

export default function HomeCarousel() {
return (
    <div >
    <Carousel className='vh-75 w-100 d-inline-block mb-5 shadow' fade>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit  carousel-height carousel-caption-bg"
        src="https://images.triumphmotorcycles.co.uk/media-library/images/central%20marketing%20team/dealer-search/become%20a%20dealer/bad-media-caorusel-3-1920x1080.jpg"
        alt="First slide"
        />
        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25 '>
        <div  className='carousel-text'>
        <h1>Gift yourself a dream bike!</h1>
        <p>Buy the most reliable motor products in Go ARVS motorcycle using your computer or smartphone</p>
        </div>
        </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src="https://content3.jdmagicbox.com/comp/remote/q7/9999p8472.8472.140317141224.n7q7/catalogue/famous-motor-two-wheeler-multi-brand-aland-gulbarga-motorcycle-dealers-honda-iuq4kq1tv5.jpg"
        alt="Second slide"
        />

        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25'>
        <div  className='carousel-text'>
        <h1>Life is incredible with new technology</h1>
        <p>Get yourself updated with the latest and quality motor vehicle in the market</p>
        </div>
        </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src="https://i.ytimg.com/vi/D0mvmafWvlQ/maxresdefault.jpg"
        alt="Third slide"
        />

        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25'>
        <div  className='carousel-text'>
        <h1>Ran out of cash?</h1>
        <p>
            Don't worry, we got you. Use your E-wallet instead or Installment!
        </p>
        </div>
        </Carousel.Caption>
    </Carousel.Item>
    </Carousel>
    </div>
);
}

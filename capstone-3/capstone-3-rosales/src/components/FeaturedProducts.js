import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function FeaturedProducts() {
    return (
        <div className='d-flex flex-column my-5 banner-bg p-5 text-white '>
        <h2>Check your favorite brands</h2>
        <Row className="my-3 text-dark h-100">
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height" >
                <Card.Img  variant="top" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Honda_Logo.svg/2552px-Honda_Logo.svg.png" />
                <Card.Header>
                <Card.Title>Honda</Card.Title>
                </Card.Header>
                <Card.Body>
                    <Card.Text>
                    Honda motorcycles are renowned for their reliability, versatility, and comfort, which makes them an excellent choice for families. With a wide range of models to choose from, a Honda motorcycle is suitable for every type of family. Here are some of the best Honda motorcycles for families
                    </Card.Text>
                </Card.Body>
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src="https://cf.shopee.ph/file/6f41fe04d45f5008d5c82fb8f30f509b" />
                <Card.Header>
                    <Card.Title>Yamaha</Card.Title>
                    </Card.Header>
                <Card.Body>
                    <Card.Text>
                    Yamaha bikes are quality products with in-built features that improve riding. The motorcycles by Yamaha bring an elegant look, phenomenal performance, finished body panels, and good performance
                    </Card.Text>
                </Card.Body>
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src="https://1000logos.net/wp-content/uploads/2020/06/Kawasaki-Logo-1967.jpg" />
                <Card.Header>
                    <Card.Title>Kawasaki</Card.Title>
                    </Card.Header>
                <Card.Body>
                    <Card.Text>
                    Stylish and rugged, Kawasaki is all about delivering fun to those who work hard and play hard. Kawasaki motorcycles provide high-quality suspension, superior brakes, ABS, and traction control to deliver speed, power, and maximum control.
                    </Card.Text>
                </Card.Body>
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Suzuki_Motor_Corporation_logo.svg/2560px-Suzuki_Motor_Corporation_logo.svg.png"/>
                <Card.Header>
                    <Card.Title>Suzuki</Card.Title>
                    </Card.Header>
                <Card.Body>
                    <Card.Text>
                Suzuki bikes are known to last and have fewer breakdowns compared to other models. Although every bike requires regular maintenance to enjoy its reliability, Suzuki consumers attest to the reliability aspect of the brand, particularly when the models are used for city riding purposes.
                    </Card.Text>
                </Card.Body>
                <Card.Footer className='text-center'>
                <Button className="w-50" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
        </Row>
        </div>
    );
}
